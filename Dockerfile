FROM nginx:latest

WORKDIR /www/data/doc/
RUN apt-get -y update
COPY nginx.conf /etc/nginx/nginx.conf
COPY index.html /www/data/doc/
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
CMD [ "nginx" ]